package org.apache.rocketmq.spring.boot.starter;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.MessageListener;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;
import java.util.UUID;

@Configuration
@EnableConfigurationProperties(RocketMQProperties.class)
@Order
@Slf4j
public class RocketMQAutoConfiguration {


    @Bean
    @ConditionalOnClass(DefaultMQProducer.class)
    @ConditionalOnMissingBean(DefaultMQProducer.class)
    @ConditionalOnProperty(prefix = "spring.rocketmq", value = {"nameServer", "producer.group"})
    public DefaultMQProducer mqProducer(RocketMQProperties rocketMQProperties) throws MQClientException {
        RocketMQProperties.Producer producerConfig = rocketMQProperties.getProducer();
        String groupName = producerConfig.getGroup();
        Assert.hasText(groupName, "[spring.rocketmq.producer.group] must not be null");
        DefaultMQProducer producer = new DefaultMQProducer(producerConfig.getGroup());
        producer.setInstanceName(UUID.randomUUID().toString());
        producer.setNamesrvAddr(rocketMQProperties.getNameServer());
        producer.setSendMsgTimeout(producerConfig.getSendMsgTimeout());
        producer.setRetryTimesWhenSendFailed(producerConfig.getRetryTimesWhenSendFailed());
        producer.setRetryTimesWhenSendAsyncFailed(producerConfig.getRetryTimesWhenSendAsyncFailed());
        producer.setMaxMessageSize(producerConfig.getMaxMessageSize());
        producer.setCompressMsgBodyOverHowmuch(producerConfig.getCompressMsgBodyOverHowmuch());
        producer.setRetryAnotherBrokerWhenNotStoreOK(producerConfig.isRetryAnotherBrokerWhenNotStoreOk());
        producer.setVipChannelEnabled(producerConfig.isVipChannelEnabled());
        producer.start();
        return producer;
    }


    @Configuration
    @ConditionalOnClass(DefaultMQPushConsumer.class)
    @EnableConfigurationProperties(RocketMQProperties.class)
    @ConditionalOnProperty(prefix = "spring.rocketmq", value = "nameServer")
    @Order
    public static class ListenerContainerConfiguration implements ApplicationContextAware, InitializingBean {

        private ConfigurableApplicationContext applicationContext;

        public void afterPropertiesSet() throws Exception {
            Map<String, Object> beans = this.applicationContext.getBeansWithAnnotation(RocketMQMessageListener.class);
            if (beans != null) {
                beans.forEach((name, bean) -> {
                    try {
                        registerContainer(name, bean);
                    } catch (MQClientException e) {
                        e.printStackTrace();
                    }
                });
            }
        }

        @Resource
        private StandardEnvironment environment;

        private String getValue(String value) {
            return environment.resolvePlaceholders(value);
        }

        private void registerContainer(String name, Object bean) throws MQClientException {
            Class<?> clazz = AopUtils.getTargetClass(bean);
            if (!MessageListener.class.isAssignableFrom(bean.getClass())) {
                throw new IllegalStateException(clazz + " is not instance of " + MessageListener.class.getName());
            }
            MessageListener rocketMQListener = (MessageListener) bean;
            RocketMQMessageListener annotation = clazz.getAnnotation(RocketMQMessageListener.class);
            DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) applicationContext.getBeanFactory();
            DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer();
            String nameServer;
            if (StringUtils.isEmpty(annotation.namesrvAddr())) {
                nameServer = environment.getProperty("pring.rocketmq.nameServer");
            } else {
                nameServer = getValue(annotation.namesrvAddr());
            }
            defaultMQPushConsumer.setNamesrvAddr(nameServer);
            defaultMQPushConsumer.setConsumerGroup(getValue(annotation.consumerGroup()));
            defaultMQPushConsumer.setVipChannelEnabled(annotation.vipChannelEnabled());
            defaultMQPushConsumer.setMessageListener(rocketMQListener);
            defaultMQPushConsumer.setConsumeFromWhere(annotation.consumeFromWhere());
            defaultMQPushConsumer.setMessageModel(annotation.messageModel());
            defaultMQPushConsumer.setConsumeThreadMax(annotation.consumeThreadMax());
            defaultMQPushConsumer.setConsumeThreadMin(annotation.consumeThreadMin());
            defaultMQPushConsumer.subscribe(getValue(annotation.topic()), getValue(annotation.subExpression()));
            if (bean instanceof RocketMQConsumerLifecycleListener) {
                RocketMQConsumerLifecycleListener rocketMQConsumerLifecycleListener = (RocketMQConsumerLifecycleListener) bean;
                rocketMQConsumerLifecycleListener.prepareStart(defaultMQPushConsumer);
            }
            defaultMQPushConsumer.start();
            String id = UUID.randomUUID().toString();
            log.info("consumer start {} {}", id, defaultMQPushConsumer);
            beanFactory.registerSingleton(name + "_listener", defaultMQPushConsumer);
        }

        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext = (ConfigurableApplicationContext) applicationContext;
        }
    }


}
