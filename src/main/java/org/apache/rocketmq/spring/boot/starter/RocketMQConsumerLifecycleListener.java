package org.apache.rocketmq.spring.boot.starter;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;

public interface RocketMQConsumerLifecycleListener {
    void prepareStart(DefaultMQPushConsumer consumer);
}