package org.apache.rocketmq;

import static org.junit.Assert.assertTrue;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.MessageListener;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.spring.boot.starter.RocketMQAutoConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.boot.test.util.EnvironmentTestUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Unit test for simple App.
 */
public class AppTest {


    AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

    @Before
    public void before() {
        ctx.register(RocketMQAutoConfiguration.class);
        EnvironmentTestUtils.addEnvironment(ctx, "spring.rocketmq.nameServer=10.32.19.51:9876;10.32.19.99:9876",
                "spring.rocketmq.producer.group=my_group",
                "spring.rocketmq.producer.vipChannelEnabled=false");
        BeanDefinitionBuilder beanBuilder = BeanDefinitionBuilder.rootBeanDefinition(TestMessageListenner.class);
        ctx.registerBeanDefinition("myListener", beanBuilder.getBeanDefinition());
        ctx.refresh();
    }


    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        Assert.assertTrue(ctx.getBeansOfType(DefaultMQProducer.class).size() > 0);
    }


    /**
     * Rigorous Test :-)
     */
    @Test
    public void listens() throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        Message m = new Message();
        m.setBody("test".getBytes());
        m.setTopic("TopicTest");
        ctx.getBean(DefaultMQProducer.class).send(m);
        Assert.assertTrue(ctx.getBeansOfType(DefaultMQPushConsumer.class).size() > 0);
        Thread.sleep(10000);
    }
//    zepplin 8890
//    spark-history-port 10001
//    SPARK_MASTER_PORT 7077
//    SPARK_MASTER_WEBUI_PORT 8080
//    SPARK_WORKER_PORT 7078
//    SPARK_WORKER_WEBUI_PORT 8081
//    yarn.log.server.url 19888
//    yarn web 8088
//    yarn node 8042

}
