package org.apache.rocketmq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.boot.starter.RocketMQMessageListener;

import java.util.List;

@Slf4j
@RocketMQMessageListener(namesrvAddr = "10.32.19.51:9876;10.32.19.99:9876", topic = "TopicTest", consumerGroup = "TopicTest_group",vipChannelEnabled = false)
public class TestMessageListenner implements MessageListenerConcurrently {
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        msgs.forEach(x -> log.info("*********{}",x.toString()));
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
